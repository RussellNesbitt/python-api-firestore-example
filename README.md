Description:

This project is an example of how to write a simple RESTful API in Python using Flask, and then set up the resulting app to use an existing Firestore Database or local Firestore Database Emulator without needing to significantly change the source code when switching between the real database and the emulator.

Built With:

All source code was written in Python as a FLASK app, and the marshmallow library was used for interpreting, constructing, and returning JSON data via its Schema system. All interaction with Firebase is handled through the firebase_admin SDK.

Functionality:

This project consists of a Flask app implementing the standard REST functions of GET, POST, PUT, and DELETE on a Firestore database containing JSON representations of the provided "Bird" object. This object consists of a numerical ID (used as the unique identifier value), a String Name, and a String Color. If a command is successful, either a JSON representation of the object is returned (POST/PUT), all objects are returned (GET), or a message confirming success is returned (DELETE). Otherwise, an error message is returned.

To Change Between an Existing Firestore Project and the Firestore Emulator:

The code is currently set up to write to a Firestore Emulator running on port 8080, with said emulator installed into the same directory for convenience. Running "Firebase init" in the directory is not required, as all non-emulator firebase functionality is provided through the source code. To write to the Firestore Emulator, first start the emulator as normal - 8080 should be the default port - then run the Flask app with birds.py. This will need to be done in a second instance, as if you are running them through Ubuntu or the command terminal, the active terminal will not accept commands until the emulator or app terminates. You can now access the REST functions using the port provided by the Flask app and the app.route specified for each function. In this case, any function can be accessed through Postman with the URL http://127.0.0.1:5000/BirdData/birds. Alternatively, the Swagger UI can be accessed through http://127.0.0.1:5000.Any data posted will be visible in the Firebase emulator's UI under Firestore, found at the URL autogenerated when the emulators are started. If you instead want to link the app to a Firebase Project's Firestore database, comment out lines 13 and 14, and restart the Flask app. Running the functions will now write the data to this Firebase Project:

https://console.firebase.google.com/project/databasetestproject-bb559/overview

The data will be viewable under the Firestore Database tab. If you want to use the app to instead write data to your own Firestore Project, you will need to generate your own firebase-adminsdk.json file for that project, and replace the existing birds-admin-key.json file with it. To return to emulator use, simply uncomment lines 13 and 14.

All testing is done through the included pytest tests located at test/test_api.py. Tests have 100% coverage of birds.py and birdSchema.py.


