from marshmallow import Schema, fields
from flask_restx import Namespace


birdNamespace = Namespace('BirdInfo', description='Bird related operations')
testNameSpace = Namespace('TestInfo', description='Test related operations')

class testSchema(Schema):
    testID = fields.Number()
    testName = fields.Str()
    testInfoOne = fields.Str()
    testInfoTwo = fields.Str()
    testInfoThree = fields.Str()

class birdSchema(Schema):
    ID = fields.Number()
    name = fields.Str()
    color = fields.Str()