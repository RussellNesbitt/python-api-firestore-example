import os
from flask import Flask, request
from API.schemas import birdSchema, testSchema, birdNamespace as ns1, testNameSpace as ns2
from flask_restx import Resource
from database.models import birdModel, birdIDModel, testModel, testIDModel
from database.db import db


#The API
@ns1.route('/birds')
class myResource(Resource):

    #GET Function: Return all objects from database
    @ns1.response(200, 'Success')
    def get(self):
        '''GET All Birds From Database'''   
        docs = db.collection("birds").stream()
        returnJson = []

        for doc in docs:
            returnJson.append(doc.to_dict())
    
        if not returnJson:
            return "No Data in Firestore Database", 200

        return returnJson, 200


    #POST Function: Add new object to database
    @ns1.response(200, 'Success')
    @ns1.response(400, 'Validation Error')
    @ns1.expect(birdModel)
    #@ns.param('ID', 'The Unique Identifier for a Bird')
    #@ns.param('name', 'The name of a Bird')
    #@ns.param('color', 'The color of a Bird')
    def post(self):
        '''POST a Bird to the Database'''
        data = request.get_json()
        schema = birdSchema()
        result = schema.load(data)

        bID = result.get("ID")
  
        doc_ref = db.collection("birds").document(str(bID))

        doc = doc_ref.get()
        if doc.exists:
            return "POST Failed: ID already in Firestore Database", 400
        else:
            doc_ref.set(result)
            return result, 200


    #PUT Function: Change an object in the database
    @ns1.response(200, 'Success')
    @ns1.response(400, 'Validation Error')
    @ns1.expect(birdModel)
    def put(self):
        '''PUT (Update) a Bird on the Database'''
        data = request.get_json()
        schema = birdSchema()
        result = schema.load(data)

        bID = result.get("ID")

        doc_ref = db.collection("birds").document(str(bID))

        doc = doc_ref.get()
        if doc.exists:
            doc_ref.set(result)
            return result, 200
        else:
            return "PUT Failed: ID does not exist in Firestore Database", 400


    #DELETE Function: Remove an object from the database
    @ns1.response(200, 'Success')
    @ns1.response(400, 'Validation Error')
    @ns1.expect(birdIDModel)
    def delete(self):
        '''DELETE a Bird on the Database'''
        data = request.get_json()
        schema = birdSchema()
        result = schema.load(data)

        bID = result.get("ID")

        doc_ref = db.collection("birds").document(str(bID))

        doc = doc_ref.get()
        if doc.exists:
            doc_ref.delete()
            return "DELETE Successful", 200
        else:
            return "DELETE Failed: ID does not exist in Firestore Database", 400


#The API
@ns2.route('/tests')
class myResource(Resource):

    #GET Function: Return all objects from database
    @ns2.response(200, 'Success')
    def get(self):
        '''GET All Tests From Database'''      
        docs = db.collection("tests").stream()
        returnJson = []

        for doc in docs:
            returnJson.append(doc.to_dict())
    
        if not returnJson:
            return "No Data in Firestore Database", 200

        return returnJson, 200


    #POST Function: Add new object to database
    @ns2.response(200, 'Success')
    @ns2.response(400, 'Validation Error')
    @ns2.expect(testModel)
    def post(self):
        '''POST a Test to the Database'''
        data = request.get_json()
        schema = testSchema()
        result = schema.load(data)

        bID = result.get("testID")
  
        doc_ref = db.collection("tests").document(str(bID))

        doc = doc_ref.get()
        if doc.exists:
            return "POST Failed: ID already in Firestore Database", 400
        else:
            doc_ref.set(result)
            return result, 200


    #PUT Function: Change an object in the database
    @ns2.response(200, 'Success')
    @ns2.response(400, 'Validation Error')
    @ns2.expect(testModel)
    def put(self):
        '''PUT (Update) a Test on the Database'''
        data = request.get_json()
        schema = testSchema()
        result = schema.load(data)

        bID = result.get("testID")

        doc_ref = db.collection("tests").document(str(bID))

        doc = doc_ref.get()
        if doc.exists:
            doc_ref.set(result)
            return result, 200
        else:
            return "PUT Failed: ID does not exist in Firestore Database", 400


    #DELETE Function: Remove an object from the database
    @ns2.response(200, 'Success')
    @ns2.response(400, 'Validation Error')
    @ns2.expect(testIDModel)
    def delete(self):
        '''DELETE a Test on the Database'''
        data = request.get_json()
        schema = testSchema()
        result = schema.load(data)

        bID = result.get("testID")

        doc_ref = db.collection("tests").document(str(bID))

        doc = doc_ref.get()
        if doc.exists:
            doc_ref.delete()
            return "DELETE Successful", 200
        else:
            return "DELETE Failed: ID does not exist in Firestore Database", 400