from flask_restx import Api
from flask import Flask
from routes import ns2 as testns, ns1 as birdns

app = Flask(__name__)

api = Api(app,
            version = "1.0",
            title = "Multiple Api",
            description = "Includes BOTH bird and test"
        )

api.add_namespace(testns)
api.add_namespace(birdns)

