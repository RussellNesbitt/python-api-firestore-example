import json
import pytest
from app import app

@pytest.fixture
def client():
    with app.test_client() as client:
        yield client



#Test GET from an empty database
def test_get_empty(client):
    response = app.test_client().get('/TestInfo/tests')
    assert response.status_code == 200
    assert b"No Data in Firestore Database" in response.data

#Test GET from a full database
def test_get_full(client):

    data = {"testID": 1, "testName": "T1","testInfoOne": "T1Inf1", "testInfoTwo": "T1Inf2", "testInfoThree": "T1Inf3"}  
    app.test_client().post('/TestInfo/tests', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)

    response = app.test_client().get('/TestInfo/tests')
    res = json.loads(response.data.decode('utf-8'))

    data = {"testID": 1, "testName": "T1","testInfoOne": "T1Inf1", "testInfoTwo": "T1Inf2", "testInfoThree": "T1Inf3"} 
    assert data in res

    assert response.status_code == 200

    deleteData = {"testID": 1}
    app.test_client().delete('/TestInfo/tests', data=json.dumps(deleteData), headers={"Content-Type": "application/json"},)

#Test POSTing to the database
def test_post(client):
    data = {"testID": 1, "testName": "T1","testInfoOne": "T1Inf1", "testInfoTwo": "T1Inf2", "testInfoThree": "T1Inf3"}  
    response = app.test_client().post('/TestInfo/tests', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 200
    res = json.loads(response.data.decode('utf-8'))
    assert res['testID'] == 1
    assert res['testName'] == 'T1'
    assert res['testInfoOne'] == 'T1Inf1'
    assert res['testInfoTwo'] == 'T1Inf2'
    assert res['testInfoThree'] == 'T1Inf3'

    deleteData = {"testID": 1}
    app.test_client().delete('/TestInfo/tests', data=json.dumps(deleteData), headers={"Content-Type": "application/json"},)

#Test failed POST attempt
def test_bad_post(client):
    data = {"testID": 1, "testName": "T1","testInfoOne": "T1Inf1", "testInfoTwo": "T1Inf2" ,"testInfoThree": "T1Inf3"}    
    response = app.test_client().post('/TestInfo/tests', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 200

    data = {"testID": 1, "testName": "T1","testInfoOne": "T1Inf1", "testInfoTwo": "T1Inf2" ,"testInfoThree": "T1Inf3"}    
    response = app.test_client().post('/TestInfo/tests', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 400
    assert b"POST Failed: ID already in Firestore Database" in response.data

    deleteData = {"testID": 1}
    app.test_client().delete('/TestInfo/tests', data=json.dumps(deleteData), headers={"Content-Type": "application/json"},)

#Test PUT
def test_put(client):
    data = {"testID": 1, "testName": "T1","testInfoOne": "T1Inf1", "testInfoTwo": "T1Inf2" ,"testInfoThree": "T1Inf3"}     
    response = app.test_client().post('/TestInfo/tests', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 200

    data = {"testID": 1, "testName": "NewT1","testInfoOne": "T1Inf1", "testInfoTwo": "T1Inf2" ,"testInfoThree": "T1Inf3"}    
    response = app.test_client().put('/TestInfo/tests', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 200
    res = json.loads(response.data.decode('utf-8'))
    assert res['testID'] == 1
    assert res['testName'] == 'NewT1'
    assert res['testInfoOne'] == 'T1Inf1'
    assert res['testInfoTwo'] == 'T1Inf2'
    assert res['testInfoThree'] == 'T1Inf3'

    deleteData = {"testID": 1}
    app.test_client().delete('/TestInfo/tests', data=json.dumps(deleteData), headers={"Content-Type": "application/json"},)

#Test Failed PUT attempt
def test_bad_put(client):
    data = {"testID": 1, "testName": "NewT1","testInfoOne": "T1Inf1", "testInfoTwo": "T1Inf2" ,"testInfoThree": "T1Inf3"}    
    response = app.test_client().put('/TestInfo/tests', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 400
    assert b"PUT Failed: ID does not exist in Firestore Database" in response.data

#Test DELETE
def test_delete(client):
    data = {"testID": 1, "testName": "NewT1","testInfoOne": "T1Inf1", "testInfoTwo": "T1Inf2" ,"testInfoThree": "T1Inf3"}      
    app.test_client().post('/TestInfo/tests', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    data = {"testID": 1} 
    response = app.test_client().delete('/TestInfo/tests', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 200
    assert b"DELETE Successful" in response.data

#Test Failed DELETE attempt
def test_bad_delete(client):
    data = {"testID": 1}
    response = app.test_client().delete('/TestInfo/tests', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 400
    assert b"DELETE Failed: ID does not exist in Firestore Database" in response.data



#BIRD TESTS
#Test GET from an empty database
def test_bird_get_empty(client):
    response = app.test_client().get('/BirdInfo/birds')
    assert response.status_code == 200
    assert b"No Data in Firestore Database" in response.data

#Test GET from a full database
def test_bird_get_full(client):

    data = {"ID": 1, "name": "Swan","color": "White"}  
    app.test_client().post('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    
    data = {"ID": 2, "name": "Crow","color": "Black"}
    app.test_client().post('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)

    response = app.test_client().get('/BirdInfo/birds')
    res = json.loads(response.data.decode('utf-8'))

    data = {"ID": 1, "name": "Swan","color": "White"} 
    assert data in res
    data = {"ID": 2, "name": "Crow","color": "Black"}
    assert data in res

    assert response.status_code == 200

    deleteData = {"ID": 1}
    app.test_client().delete('/BirdInfo/birds', data=json.dumps(deleteData), headers={"Content-Type": "application/json"},)
    deleteData = {"ID": 2}
    app.test_client().delete('/BirdInfo/birds', data=json.dumps(deleteData), headers={"Content-Type": "application/json"},)

#Test POSTing to the database
def test_bird_post(client):
    data = {"ID": 1, "name": "Swan","color": "White"}   
    response = app.test_client().post('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 200
    res = json.loads(response.data.decode('utf-8'))
    assert res['ID'] == 1
    assert res['name'] == 'Swan'
    assert res['color'] == 'White'
    data = {"ID": 2, "name": "Crow","color": "Black"}
    response = app.test_client().post('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 200
    res = json.loads(response.data.decode('utf-8'))
    assert res['ID'] == 2
    assert res['name'] == 'Crow'
    assert res['color'] == 'Black'

    deleteData = {"ID": 1}
    app.test_client().delete('/BirdInfo/birds', data=json.dumps(deleteData), headers={"Content-Type": "application/json"},)
    deleteData = {"ID": 2}
    app.test_client().delete('/BirdInfo/birds', data=json.dumps(deleteData), headers={"Content-Type": "application/json"},)

#Test failed POST attempt
def test_bird_bad_post(client):
    data = {"ID": 1, "name": "Swan","color": "White"}   
    response = app.test_client().post('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 200
    res = json.loads(response.data.decode('utf-8'))
    assert res['ID'] == 1
    assert res['name'] == 'Swan'
    assert res['color'] == 'White'
    data = {"ID": 1, "name": "Crow","color": "Black"}
    response = app.test_client().post('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 400
    assert b"POST Failed: ID already in Firestore Database" in response.data

    deleteData = {"ID": 1}
    app.test_client().delete('/BirdInfo/birds', data=json.dumps(deleteData), headers={"Content-Type": "application/json"},)

#Test PUT
def test_bird_put(client):
    data = {"ID": 1, "name": "Swan","color": "White"}  
    response = app.test_client().post('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 200
    res = json.loads(response.data.decode('utf-8'))
    assert res['ID'] == 1
    assert res['name'] == 'Swan'
    assert res['color'] == 'White'

    data = {"ID": 1, "name": "Crow","color": "Black"}
    response = app.test_client().put('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 200
    res = json.loads(response.data.decode('utf-8'))
    assert res['ID'] == 1
    assert res['name'] == 'Crow'
    assert res['color'] == 'Black'

    deleteData = {"ID": 1}
    app.test_client().delete('/BirdInfo/birds', data=json.dumps(deleteData), headers={"Content-Type": "application/json"},)

#Test Failed PUT attempt
def test_bird_bad_put(client):
    data = {"ID": 1, "name": "Crow","color": "Black"}
    response = app.test_client().put('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 400
    assert b"PUT Failed: ID does not exist in Firestore Database" in response.data

#Test DELETE
def test_bird_delete(client):
    data = {"ID": 1, "name": "Crow","color": "Black"}    
    app.test_client().post('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    data = {"ID": 1} 
    response = app.test_client().delete('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 200
    assert b"DELETE Successful" in response.data

#Test Failed DELETE attempt
def test_bird_bad_delete(client):
    data = {"ID": 1}
    response = app.test_client().delete('/BirdInfo/birds', data=json.dumps(data),
        headers={"Content-Type": "application/json"},)
    assert response.status_code == 400
    assert b"DELETE Failed: ID does not exist in Firestore Database" in response.data