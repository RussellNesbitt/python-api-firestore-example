from API.schemas import testNameSpace as tns, birdNamespace as bns
from flask_restx import fields

testModel = tns.model('Test Model', {
    'testID': fields.Integer(required = True, description="ID of the Test", help="ID cannot be blank."),
    'testName': fields.String(required = True, description="Name of the Test", help="Name cannot be blank."),
    'testInfoOne': fields.String(required = True, description="First Info of the Test", help="First Info cannot be blank."),
    'testInfoTwo': fields.String(required = True, description="Second Info of the Test", help="Second Info cannot be blank."),
    'testInfoThree': fields.String(required = True, description="Third Info of the Test", help="Third Info cannot be blank.")
    })

testIDModel = tns.model('Test ID Model', {
    'testID': fields.Integer(required = True, description="ID of the Test", help="ID cannot be blank.")
    })

birdModel = bns.model('Bird Model', {
    'ID': fields.Integer(required = True, description="ID of the Bird", help="ID cannot be blank."),
    'name': fields.String(required = True, description="Name of the Bird", help="Name cannot be blank."),
    'color': fields.String(required = True, description="Color of the Bird", help="Color cannot be blank.")
    })

birdIDModel = bns.model('Bird ID Model', {
    'ID': fields.Integer(required = True, description="ID of the Bird", help="ID cannot be blank.")
    })