import os
from firebase_admin import credentials, firestore, initialize_app

#These two lines connect the app to the local Firestore Emulator
#removing them instead connects the app to the online Firestore Database associated with the
#project described in the downloadable admin-key.json file.
os.environ["GCLOUD_PROJECT"] = "databasetestproject-bb559"
os.environ["FIRESTORE_EMULATOR_HOST"] = "localhost:8080"

cred = credentials.Certificate('mykey.json')
default_app = initialize_app(cred)

db = firestore.client()